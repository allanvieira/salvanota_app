angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('tabsController.doarNota', {
    url: '/doar',
    views: {
      'tab1': {
        templateUrl: 'templates/doarNota.html',
        controller: 'doarNotaCtrl'
      }
    }
  })

  .state('tabsController.inCio', {
    url: '/inicio',
    views: {
      'tab2': {
        templateUrl: 'templates/inCio.html',
        controller: 'inCioCtrl'
      }
    }
  })

  .state('tabsController.entidadesCadastradas', {
    url: '/entidades',
    views: {
      'tab3': {
        templateUrl: 'templates/entidadesCadastradas.html',
        controller: 'entidadesCadastradasCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('detalhesDaEntidade', {
    url: '/detalhes-entidade',
    templateUrl: 'templates/detalhesDaEntidade.html',
    controller: 'detalhesDaEntidadeCtrl'
  })

$urlRouterProvider.otherwise('/page1/entidades')


});